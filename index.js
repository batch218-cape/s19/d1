// console.log("Hello World");

// What are conditional statements?
// Conditional statements allow us to control the flow of our program and it allow us to run statement/instruction based on the condition.

// if statement
// Executes a statement if a specified condition is true
/*
    //Syntax
    if (condition) {
        code block / statements;
    }

 */   

let numA = 0;

if (numA < 0) {
    console.log("Hello");
}
// The codeblock inside the if statement will not work it doesn't satisfy the condition

if (numA == 0) {
    console.log("Hello");
}

//The reult of the expression must result to "true", else it will not run the statement inside.
console.log(numA < 0); //false
console.log(numA == 0); //true

let city = "New York";
if (city == "New York") {
    console.log("Welcome to New York City");
}

// else if clause
/*
    -Executes a statement if previous condition/s are false and if the specified condition is true
    -The "else if" clause is optional and can be added to captire additonal conditions to chande the flow of the program
*/

let numH = 0;

if (numH < 0) {  // returned false
    console.log("The number is negative");
} else if (numH > 0) {  //returned false
    console.log("The number is positive");
} else if (numH == 0) {
    console.log("The number is zero");
}

// City = "New York";
if (city === "New York") {
    console.log("Welcome to New York City");
} else if(city === "Tokyo") {
    console.log("Welcome to Tokyo, Japan");
}

//Alternative code using else statement
if (numH < 0) {   //returned false
    console.log("The number is negative");
} else if (numH > 0) {  //returned false
    console.log("The number is positive");
} else {
    console.log("The number is zero");
}

//else statement 
/*
    - Executes a statement if all other conditions are false.
    - The 'else' statement is optional and can be added to capture any other result to change the flow of the program
*/

if (city === "New York") {
    console.log("Welcome to New York City");
} else if(city === "Tokyo") {
    console.log("Welcome to Tokyo, Japan");
} else {
    console.log("City is not included in the list");
}

//----------------------------------------------------------------

let message = "No message";
	console.log(message);

	function determineTyphooneIntensity(windSpeed){

		if(windSpeed < 30){
			return "Not a typhoon yet."
		}
		else if(windSpeed <= 61){
			return "Tropical depression detected."
		}
		else if (windSpeed >= 62 && windSpeed <= 88){
			return "Tropical storm detected."
		}
		else if(windSpeed >= 89 && windSpeed <= 117){
			return "Severe Tropical storm deteceted";
		}
		else{
			return "Typhoon detected."
		}
	}

	message = determineTyphooneIntensity(110);
	console.log(message);

	//  console.warn() is a good way to print warnings in our console that could help us developers act on certain output within our code.
	if(message === "Severe Tropical storm detected"){
		console.warn(message);
	}

//[SECTION] Truthy and False
/*
    -In javascript a "truthy" value is consdered true when encountered in a boolean context
    -False
        1. false
        2. 0
        3. "" //empty
        4. null //also an empty variable or declared as null
        5. undefined //let number; 
        6. Nan (Not a Number)


*/

let isMarried = true;

//Truthy examples
if (true) {
    console.log("Truthy");
}
if (1) {
    console.log("Truthy");
}
if ([]) {
    console.log("Truthy");
}

// Falsy examples 
if (false) {
    console.log("Falsy");
}
if (0) {
    console.log("Falsy");
}
if (undefined) {
    console.log("Falsy");
}
if (isMarried) {
    console.log("Truthy");
}

// [SECTION] Conditional (Ternary) Operator
/*
    -The conditional (Ternary operator takes in three operands)
*/

let ternaryResult = (1 < 18) ? true : false;
console.log("Result of Ternary Operator: " + ternaryResult);

// Multiple statement execution using ternary operator
let name;

function isOfLegalAge () {
    name = "John";
    return "You are in legal age";
}

function underAge () {
    name = "Jane";
    return "You are under age limit";
}

// parseInt - converts the input received into a number datatype
    // if a input is not a number, it will return NaN (Not a Number)
// let age = parseInt (prompt("What is your age"))
// console.log(age);

// let legalAge = (age >= 18) ? isOfLegalAge() : underAge();
// console.log("Return of Ternary Operator in functions " + legalAge + ', ' + name);

// [SECTION] Switch Statements

let day = prompt ("What day of the week is it today").toLowerCase();
console.log(day); // display input

switch(day) {
    case "monday":
        console.log("The colour of the day is red"); // display output
        break;

    case "tuesday":
        console.log("The colour of the day is orange");
        break;

    case "wednesday":
        console.log("The colour of the day is yellow");
        break;

    case "thursday":
        console.log("The colour of the day is green");
        break;

    case "friday":
        console.log("The colour of the day is blue");
        break;

    case "saturday":
        console.log("The colour of the day is indigo");
        break;

    case "sunday":
        console.log("The colour of the day is violet");
        break;

    default:
        console.log("Please input a valid day");
}

let age = prompt("age: ");
age = parseInt(age);

// switch(age) {
//     case 17:
//         console.log("Age is invalid");
//         break;

//     case 18:
//         console.log("Age is valid");

//     case 18:
//         console.log("Give freebies");
// }

// [SECTION] Try-Catch-Finally Statement

let codeErrorMessage = "Code Error";

function showIntensityAlert(windSpeed) {
    try {
        alerat("Sample AlertMessage"); // Error on purpose
    } catch (error) {
        console.warn(error.message);
    } finally {
        alert("Intensity will show new alert")
    }
}

showIntensityAlert(56);

console.log("Sample output after try-catch-finally block");




function displayInstructor(name){
    return "Instructor " + name;
}

displayInstructor("Topher");




function displayInstructor(name){
    return "Instructor " + name;
}

instructor = displayInstructor("Topher");
console.log(instructor);

